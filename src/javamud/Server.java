package javamud;

import java.net.*;

import javamud.world.World;

import java.io.*;

public class Server {

	static World game;

	static ServerSocket servSock;

	public static final int SERVER_PORT = 9991;

	public static final int NUM_CONNECTION_THREADS = 4;
	
	public static long serverStartTime = System.currentTimeMillis();

	public static int connections = 0;
	
	public static void main(String[] av) {
		new Server(SERVER_PORT, NUM_CONNECTION_THREADS);
	}

	public Server(int port, int numThreads) {

		try {
			servSock = new ServerSocket(port);
			game = new World();
			game.loadTiles();
		} catch (IOException e) {
			/* Crash the server if IO fails. Something bad has happened */
			throw new RuntimeException("Could not create ServerSocket ", e);
		}

		// Create a series of threads and start them.
		/*for (int i = 0; i < numThreads; i++) { // TODO start and kill threads as needed
			new Connection(servSock, i).start();
		}*/
		new Connection(servSock, getConnections()).start();
	}
	
	public static void startNewConnection() {  //TODO some finna pooling/reusing disconnected slots
		connections++;
		new Connection(servSock, getConnections()).start();
	}
	public static int getConnections() {
		return connections;
	}

	public static World getWorld() {
		return game;
	}
}