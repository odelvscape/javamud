package javamud;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import javamud.players.Player;
import javamud.players.saves.LoadPlayer;
import javamud.players.saves.SavePlayer;

public class Connection extends Thread {
	Socket clientSocket;
	ServerSocket servSock;
	Player p;
	PrintStream os;
	int threadNumber;
	boolean loggedIn = false;

	Connection(ServerSocket s, int i) {
		servSock = s;
		threadNumber = i;
		setName("Thread " + threadNumber);
	}

	public PrintStream getOutStream() {
		return os;
	}

	public void run() {
		/*
		 * Wait for a connection. Synchronized on the ServerSocket while calling its
		 * accept() method.
		 */
		while (true) {
			try {
				System.out.println(getName() + " waiting");
				// Wait here for the next connection.
				synchronized (servSock) {
					clientSocket = servSock.accept();
				}
				System.out.println(getName() + " Incoming connection from IP " + clientSocket.getInetAddress());
				Server.startNewConnection();  //Start the connection for the next player
				BufferedReader is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				os = new PrintStream(clientSocket.getOutputStream(), true);
				String line;
				os.print("\n");
				os.print("Welcome to JavaMUD!" + "\n");
				os.print("By Odel" + "\n");
				os.print("\n");
				os.print("Please log in" + "\n");
				os.print("Username: ");
				Player p = null;
				while ((line = is.readLine()) != null) {
					line = line.replace("\n", "");
					if (!loggedIn) {
						if (Server.getWorld().getIsPlayerLoggedIn(line)) {
							os.print("That Username is already logged in..." + "\r\n");
						} else if (fileExists(line)) {
							p = new Player(line, this, clientSocket.getInetAddress().toString());
							os.print("Oh hi, " + line + "\r\n"); // TODO check password for login
							LoadPlayer l = new LoadPlayer();
							l.loadPlayer(p);
							p.logIntoWorld();
							loggedIn = true;
						} else { // TODO new character creation
							os.print("Not a valid username" + "\r\n"); // TODO character creation
						}
					} else {
						p.getInputParser().parse(line);
					}
					if (!loggedIn) {
						os.print("Username: ");
					} else {
						p.printPrompt();
					}
					os.flush();
				}
				disconnect();
				System.out.println(getName() + " ENDED ");
				clientSocket.close();
			} catch (IOException ex) {
				System.out.println(getName() + ": IO Error on socket " + ex);
				return;
			}
		}

	}

	public void disconnect() {
		if (p != null) {
			if (p.getCurrentTile() != null) {
				SavePlayer s = new SavePlayer();
				s.writeSave(p);
				p.leaveTile(p.getCurrentTile(), true);
			}
		}
		System.out.println(getName() + " ENDED ");
		loggedIn = false;
		p = null;
	}

	public static final String saveDir = "./data/players/";

	public boolean fileExists(String name) {
		File file = new File(saveDir + name + ".txt");
		if (file.exists()) {
			return true;
		}
		return false;
	}
}
