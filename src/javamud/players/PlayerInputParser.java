package javamud.players;

import javamud.Server;
import javamud.players.saves.SavePlayer;

public class PlayerInputParser {

	Player p;

	public PlayerInputParser(Player player) {
		p = player;
	}

	public void parse(String in) { // TODO write a shutdown and save command after doing char saving
		if (in.equalsIgnoreCase("logout") || in.equalsIgnoreCase("quit") || in.equalsIgnoreCase("exit")) {
			p.logOutOfWorld();
		}
		if (in.equalsIgnoreCase("news")) {
			p.print("This world is not yet advanced enough to have any perodical publications" + "\r\n");
		}
		if (in.equalsIgnoreCase("uptime")) {
			long timesec = (System.currentTimeMillis() - Server.serverStartTime) / 1000;
			long timemin = timesec / 60;
			p.print("The server has been up " + timemin + " minutes" + "\r\n");
		}
		if (in.equalsIgnoreCase("whoami")) {
			p.print("You are " + p.getName() + ", of course!" + "\r\n");
		}
		if (in.equalsIgnoreCase("who") || in.equalsIgnoreCase("players")) {
			p.printPlayerCount();
			p.printPlayersOnline();
		}
		if (in.equalsIgnoreCase("look")) {
			p.printLook(true);
		}
		if (in.equalsIgnoreCase("exits")) {
			p.printLook(true);
		}
		if (in.toLowerCase().startsWith("say")) {
			for (Player pl : p.getCurrentTile().getPlayers()) {
				if (p != pl) {
					pl.print(p.getName() + " says " + in.substring(4) + "\r\n");
					pl.printPrompt();
				}
			}
		}
		if (in.equalsIgnoreCase("n") || in.equalsIgnoreCase("north")) {
			if (p.getCurrentTile().getExit("North") != null) {
				p.print("You head North" + "\r\n");
				p.moveTo(p.getCurrentTile().getExit("North"), false);
			} else {
				p.print("There is no North exit!" + "\r\n");
			}
		}
		if (in.equalsIgnoreCase("s") || in.equalsIgnoreCase("south")) {
			if (p.getCurrentTile().getExit("South") != null) {
				p.print("You head South" + "\r\n");
				p.moveTo(p.getCurrentTile().getExit("South"), false);
			} else {
				p.print("There is no South exit!" + "\r\n");
			}
		}
		if (in.equalsIgnoreCase("e") || in.equalsIgnoreCase("east")) {
			if (p.getCurrentTile().getExit("East") != null) {
				p.print("You head East" + "\r\n");
				p.moveTo(p.getCurrentTile().getExit("East"), false);
			} else {
				p.print("There is no East exit!" + "\r\n");
			}
		}
		if (in.equalsIgnoreCase("w") || in.equalsIgnoreCase("west")) {
			if (p.getCurrentTile().getExit("West") != null) {
				p.print("You head West" + "\r\n");
				p.moveTo(p.getCurrentTile().getExit("West"), false);
			} else {
				p.print("There is no West exit!" + "\r\n");
			}
		}
	}
}
