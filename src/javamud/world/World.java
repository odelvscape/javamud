package javamud.world;

import java.util.ArrayList;

import javamud.players.Player;

public class World {
	private ArrayList<Player> players = new ArrayList<Player>();
	private ArrayList<GameTile> tiles = new ArrayList<GameTile>();

	public World() {

	}

	public void loadTiles() { // TODO make a file format and loader for world data
		for (int x = 0; x < 7; x++) {
			tiles.add(new GameTile(x, "test tile " + x, "this is test tile, welcome"));
		}
		tiles.get(0).addExit("North", tiles.get(1));
		tiles.get(1).addExit("South", tiles.get(0));
		tiles.get(1).addExit("East", tiles.get(2));
		tiles.get(1).addExit("West", tiles.get(5));
		tiles.get(2).addExit("West", tiles.get(1));
		tiles.get(2).addExit("East", tiles.get(3));
		tiles.get(3).addExit("West", tiles.get(2));
		tiles.get(4).addExit("South", tiles.get(1));
		tiles.get(5).addExit("North", tiles.get(6));
		tiles.get(6).addExit("East", tiles.get(4));

	}

	public GameTile getTile(int t) {
		return tiles.get(t);
	}

	public boolean getIsPlayerLoggedIn(String name) {
		for (Player p : players) {
			if (p.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public int getPlayerCount() {
		return players.size();
	}

	public void logPlayerIn(Player p) {
		registerPlayer(p);
		p.moveTo(p.getCurrentTile(), true);
		System.out.println(p.getName() + " has logged in " + p.getIP());
	}

	public void registerPlayer(Player p) {
		players.add(p);
	}

	public void logPlayerOut(Player p) {
		players.remove(p);
	}
}
