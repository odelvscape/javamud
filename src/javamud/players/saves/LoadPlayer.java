package javamud.players.saves;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javamud.players.Player;

public class LoadPlayer {

	public static final String saveDir = "./data/players/";

	// TODO health, items etc from a file on login
	// Some finna better file format, zipped json?
	public void loadPlayer(Player p) {
		File file = new File(saveDir + p.getName() + ".txt");
		if (file.exists()) {
			String line = "";
			String token = "";
			String token2 = "";
			String section = "";
			try (BufferedReader reader = new BufferedReader(new FileReader(saveDir + p.getName() + ".txt"))) {
				line = reader.readLine();
				while (line != null) {
					line = line.trim();
					if (line.startsWith("[") && line.endsWith("]")) {
						section = line;
					}
					int equalIndex = line.indexOf("=");
					if (equalIndex > -1) {
						token = line.substring(0, equalIndex);
						token = token.trim();
						token2 = line.substring(equalIndex + 1);
						token2 = token2.trim();
						if (!token2.isEmpty() && token2.length() > 0) {
							switch (section) {
							case "[CHARACTER]":
								switch (token) {
								case "username":
									System.out.println(token2);
									break;
								case "password":
									System.out.println(token2);
									break;
								case "location":
									System.out.println(token2);
									p.setCurrentTile(Integer.parseInt(token2));
									break;
								}
								break;
							}
						}
					}
					line = reader.readLine();
				}
				return;
			} catch (IOException ioexception) {
				System.out.println("error writing settings file.");
			}
		}
		return;
	}
}
