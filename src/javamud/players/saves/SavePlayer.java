package javamud.players.saves;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javamud.players.Player;

public class SavePlayer {

	public static final String saveDir = "./data/players/";

	// TODO health, items etc from a file on login
	// Some finna better file format, zipped json?
	public void writeSave(Player p) {
		File file = new File(saveDir + p.getName() + ".txt");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (IOException ioexception) {
			System.out.println("error writing settings file.");
		}

		if (file.exists()) {
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(saveDir + p.getName() + ".txt"))) {
				writer.write("[CHARACTER]");
				writer.newLine();
				writer.write("username = " + p.getName());
				writer.newLine();
				writer.write("password = " + "password");
				writer.newLine();
				writer.write("location = " + p.getCurrentTile().getID());
				writer.newLine();
				writer.flush();
				writer.close();
				System.out.println("Just saved " + p.getName());
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}
