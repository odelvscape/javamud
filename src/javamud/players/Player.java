package javamud.players;

import javamud.Connection;
import javamud.Server;
import javamud.world.GameTile;

public class Player {
	private String name = "";
	private String ip = "";
	private Connection conn;
	private PlayerInputParser parser;
	private GameTile current;

	public Player(String n, Connection connection, String i) {
		name = n;
		conn = connection;
		ip = i;
		parser = new PlayerInputParser(this);
	}

	public void logIntoWorld() {
		Server.getWorld().logPlayerIn(this);
		current = Server.getWorld().getTile(current.getID());
	}

	public GameTile getCurrentTile() {
		return current;
	}

	public void setCurrentTile(GameTile t) {
		current = t;
	}

	public void setCurrentTile(int tileID) {
		System.out.println("Logging into " + tileID);
		current = Server.getWorld().getTile(tileID);
	}

	public void leaveTile(GameTile t, boolean disconnect) {
		if (current.getPlayers().size() > 1) {
			for (Player p : current.getPlayers()) {
				if (p != this) {
					if (!disconnect) {
						p.print(getName() + " leaves" + "\r\n");
					} else {
						p.print(getName() + " vanishes into thin air!" + "\r\n");
					}
					p.printPrompt();
				}
			}
		}
		current.removePlayer(this);
	}

	public void moveTo(GameTile t, boolean login) {
		if (current != null) {
			leaveTile(t, false);
		}
		setCurrentTile(t);
		current.addPlayer(this);
		if (current.getPlayers().size() > 1) {
			for (Player p : current.getPlayers()) {
				if (p != this) {
					if (!login) {
						p.print(getName() + " appears" + "\r\n");
					} else {
						p.print(getName() + " appears out of thin air!" + "\r\n");
					}
					p.printPrompt();
				}
			}
		}
		printLook(false);
	}

	public void printExits() {
		if (current.getExits().size() > 0) {
			print("Exits: ");
			current.getExits().forEach((d, t) -> {
				print(d + " ");
			});
			print("\r\n");
		}
	}

	public void printLook(boolean desc) {
		print("Location: " + current.getName() + "\r\n");
		if (current.getPlayers().size() > 1) {
			print("You see ");
			for (Player p : current.getPlayers()) {
				if (p != this)
					print(p.getName() + " ");
			}
			print("\r\n");
		}
		if (desc) {
			print(current.getDesc() + "\r\n");
		}
		printExits();
	}

	public void logOutOfWorld() {
		leaveTile(current, true);
		Server.getWorld().logPlayerOut(this);
		conn.getOutStream().print("You've been logged out." + "\r\n");
		conn.disconnect();
	}

	public void setName(String n) {
		name = n;
	}

	public Object getName() {
		return name;
	}

	public PlayerInputParser getInputParser() {
		return parser;
	}

	public void printPlayerCount() {
		conn.getOutStream().print("There are " + Server.getWorld().getPlayerCount() + " players online." + "\r\n");
	}

	public void printPlayersOnline() {
		for (Player p : Server.getWorld().getPlayers()) {
			conn.getOutStream().print(p.getName() + " ");
		}
		conn.getOutStream().print("\r\n");
	}

	public void printPrompt() {
		conn.getOutStream().print(">>> ");
	}

	public void print(String s) {
		conn.getOutStream().print(s);
	}

	public String getIP() {
		return ip;
	}

	public Connection getConnection() {
		// TODO Auto-generated method stub
		return conn;
	}
}
