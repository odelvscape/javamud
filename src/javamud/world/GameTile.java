package javamud.world;

import java.util.ArrayList;
import java.util.Hashtable;

import javamud.players.Player;

public class GameTile {
	private ArrayList<Player> players = new ArrayList<Player>();
	Hashtable<String, GameTile> exits = new Hashtable<String, GameTile>();

	protected int idNumber;
	protected String name;
	protected String desc;

	public GameTile(int id, String n, String d) {
		idNumber = id;
		name = n;
		desc = d;
	}

	public void addExit(String dir, GameTile t) {
		exits.put(dir, t);
	}

	public GameTile getExit(String dir) {
		return exits.get(dir);
	}

	public void addPlayer(Player p) {
		players.add(p);
	}

	public void removePlayer(Player p) {
		players.remove(p);
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public Hashtable<String, GameTile> getExits() {
		return exits;
	}

	public int getID() {
		return idNumber;
	}
}
